package me.agolokoz.data.network;

import io.reactivex.Observable;
import me.agolokoz.data.entity.chat.info.ChatInfoEntitiesResponse;
import me.agolokoz.data.entity.chat.message.MessageHistoryResponseEntity;
import me.agolokoz.data.entity.dialog.DialogsResponseEntity;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MessagesServiceApi {

    @GET("method/messages.getDialogs")
    Observable<DialogsResponseEntity> requestDialogs(
            @Query("access_token") String accessToken,
            @Query("offset") int offset,
            @Query("count") int limit
    );


    @GET("method/messages.getChat")
    Observable<ChatInfoEntitiesResponse> requestChatsInfo(
            @Query("access_token") String accessToken,
            @Query("chat_ids") String chatIds
    );


    @GET("method/messages.getHistory")
    Observable<MessageHistoryResponseEntity> requestHistory(
            @Query("access_token") String accessToken,
            @Query("user_id") long userId,
            @Query("offset") int offset,
            @Query("count") Integer limit
    );

}
