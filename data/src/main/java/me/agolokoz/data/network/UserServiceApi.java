package me.agolokoz.data.network;

import io.reactivex.Observable;
import me.agolokoz.data.entity.user.UserEntitiesResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface UserServiceApi {

    @GET("method/users.get")
    Observable<UserEntitiesResponse> requestUserEntities(
            @Query("user_ids") String userIds,
            @Query("fields") String fields
    );

}
