package me.agolokoz.data.repository;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import me.agolokoz.data.entity.chat.info.ChatInfoEntitiesResponse;
import me.agolokoz.data.entity.chat.info.ChatInfoEntity;
import me.agolokoz.data.entity.chat.message.MessageEntity;
import me.agolokoz.data.entity.chat.message.MessageHistoryResponseEntity;
import me.agolokoz.data.entity.chat.message.attachment.MessageEntityAttachmentPhoto;
import me.agolokoz.data.entity.dialog.DialogEntity;
import me.agolokoz.data.entity.dialog.DialogsResponseEntity;
import me.agolokoz.data.network.MessagesServiceApi;
import me.agolokoz.data.util.PhotoSizeUtil;
import me.agolokoz.domain.model.chat.info.ChatInfo;
import me.agolokoz.domain.model.chat.info.ChatsResponse;
import me.agolokoz.domain.model.chat.message.Message;
import me.agolokoz.domain.model.chat.message.MessageHistoryResponse;
import me.agolokoz.domain.model.chat.message.attachment.MessageAttachment;
import me.agolokoz.domain.repository.MessagesRepository;
import me.agolokoz.domain.util.DateUtils;

public class MessagesRepositoryImpl implements MessagesRepository {

    private static final int chatIdPrefix = 2000000000;
    private static final int limit = 50;

    @NonNull private final CommonRepository commonRepository;
    @NonNull private final MessagesServiceApi messagesServiceApi;

    private final List<ChatInfo> chatInfoCache = new ArrayList<>();
    private int totalConversationsCount = 0;

    public MessagesRepositoryImpl(
            @NonNull CommonRepository commonRepository,
            @NonNull MessagesServiceApi messagesServiceApi
    ) {
        this.commonRepository = commonRepository;
        this.messagesServiceApi = messagesServiceApi;
    }

    @Override
    public Observable<ChatsResponse> requestConversations(int offset) {
        if (!chatInfoCache.isEmpty() && offset < chatInfoCache.size()) {
            return Observable.just(
                    new ChatsResponse(totalConversationsCount, 0,
                            chatInfoCache.subList(offset, chatInfoCache.size())
                    ));
        }

        return messagesServiceApi.requestDialogs(commonRepository.getAccessToken(), offset, limit)
                .map(getDialogsConversationsMapper());
    }

    @Override
    public Observable<SparseArray<Uri>> requestConversationsPhotoUri(int[] conversationsIds) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i != conversationsIds.length; ++i) {
            builder.append(String.valueOf(conversationsIds[i])).append(",");
        }
        builder.deleteCharAt(builder.length()-1);

        return messagesServiceApi.requestChatsInfo(commonRepository.getAccessToken(), builder.toString())
                .map(getChatEntitiesPhotoInfoMapper())
                .doOnNext(getChatsPhotoUriConsumer());
    }


    @Override
    public Observable<MessageHistoryResponse> requestChatHistory(int chatId, int offset) {
        long chatIdRequestValue = chatIdPrefix + chatId;
        return messagesServiceApi.requestHistory(commonRepository.getAccessToken(), chatIdRequestValue, offset, null)
                .map(getMessageHistoryEntityMapper());
    }

    @Override
    public Observable<ChatInfo> requestChatInfo(int chatId) {
        ChatInfo chatInfo = null;
        for (ChatInfo info : chatInfoCache) {
            if (info.getId() == chatId) {
                chatInfo = info;
                break;
            }
        }

        return chatInfo != null
                ? Observable.just(chatInfo)
                : messagesServiceApi.requestChatsInfo(commonRepository.getAccessToken(), String.valueOf(chatId)).map(getChatInfoEntitiesMapper());
    }


    private Function<DialogsResponseEntity, ChatsResponse> dialogsConversationsMapper;
    private Function<DialogsResponseEntity, ChatsResponse> getDialogsConversationsMapper() {
        if (dialogsConversationsMapper == null) {
            dialogsConversationsMapper = new Function<DialogsResponseEntity, ChatsResponse>() {
                @Override
                public ChatsResponse apply(@NonNull DialogsResponseEntity dialogsResponseEntity) throws Exception {
                    int conversationsCount = 0;
                    int skippedItems = 0;
                    for (DialogEntity entity : dialogsResponseEntity.response) {
                        if (entity.chat_id != 0) ++conversationsCount;
                        else ++skippedItems;
                    }

                    List<ChatInfo> chatInfos = new ArrayList<>(conversationsCount);
                    for (DialogEntity entity : dialogsResponseEntity.response) {
                        if (entity.chat_id == 0) continue;
                        chatInfos.add(DialogEntity.transform(entity));
                    }

                    totalConversationsCount = dialogsResponseEntity.totalDialogsCount;
                    chatInfoCache.addAll(chatInfos);
                    return new ChatsResponse(totalConversationsCount, skippedItems, chatInfos);
                }
            };
        }
        return dialogsConversationsMapper;
    }

    private Consumer<SparseArray<Uri>> chatsPhotoUriConsumer;
    private Consumer<SparseArray<Uri>> getChatsPhotoUriConsumer() {
        if (chatsPhotoUriConsumer == null) {
            chatsPhotoUriConsumer = new Consumer<SparseArray<Uri>>() {
                @Override
                public void accept(SparseArray<Uri> uriSparseArray) throws Exception {
                    for (ChatInfo chatInfo : chatInfoCache) {
                        Uri uri = uriSparseArray.get(chatInfo.getId(), null);
                        if (uri != null) {
                            chatInfo.setPhotoUri(uri);
                        }
                    }
                }
            };
        }
        return chatsPhotoUriConsumer;
    }

    private Function<ChatInfoEntitiesResponse, SparseArray<Uri>> chatEntitiesPhotoInfoMapper;
    private Function<ChatInfoEntitiesResponse, SparseArray<Uri>> getChatEntitiesPhotoInfoMapper() {
        if (chatEntitiesPhotoInfoMapper == null) {
            chatEntitiesPhotoInfoMapper = new Function<ChatInfoEntitiesResponse, SparseArray<Uri>>() {
                @Override
                public SparseArray<Uri> apply(@NonNull ChatInfoEntitiesResponse chatEntitiesResponse) throws Exception {
                    SparseArray<Uri> chatPhotoUriMap = new SparseArray<>();
                    for (ChatInfoEntity entity : chatEntitiesResponse.response) {
                        String photoUriString = getChatPhotoUriString(entity, commonRepository.getUserPhotoSize());
                        if (photoUriString == null) {
                            continue;
                        }

                        Uri photoUri = Uri.parse(photoUriString);
                        chatPhotoUriMap.put(entity.chat_id, photoUri);
                    }
                    return chatPhotoUriMap;
                }
            };
        }
        return chatEntitiesPhotoInfoMapper;
    }

    private Function<ChatInfoEntitiesResponse, ChatInfo> chatInfoEntitiesMapper;
    private Function<ChatInfoEntitiesResponse, ChatInfo> getChatInfoEntitiesMapper() {
        if (chatInfoEntitiesMapper == null) {
            chatInfoEntitiesMapper = new Function<ChatInfoEntitiesResponse, ChatInfo>() {
                @Override
                public ChatInfo apply(@NonNull ChatInfoEntitiesResponse chatInfoEntitiesResponse) throws Exception {
                    ChatInfoEntity infoEntity = chatInfoEntitiesResponse.response[0];
                    int[] firstUserIds = infoEntity.users.length < 4
                            ? infoEntity.users
                            : Arrays.copyOfRange(infoEntity.users, 0, 4);

                    ChatInfo chatInfo = new ChatInfo(infoEntity.chat_id, infoEntity.title, null, null, firstUserIds, infoEntity.users.length);
                    String photoUriString = getChatPhotoUriString(infoEntity, commonRepository.getUserPhotoSize());
                    if (photoUriString != null) {
                        chatInfo.setPhotoUri(Uri.parse(photoUriString));
                    }
                    return chatInfo;
                }
            };
        }
        return chatInfoEntitiesMapper;
    }

    private Function<MessageHistoryResponseEntity, MessageHistoryResponse> messageHistoryEntityMapper;
    private Function<MessageHistoryResponseEntity, MessageHistoryResponse> getMessageHistoryEntityMapper() {
        if (messageHistoryEntityMapper == null) {
            messageHistoryEntityMapper = new Function<MessageHistoryResponseEntity, MessageHistoryResponse>() {
                @Override
                public MessageHistoryResponse apply(@NonNull MessageHistoryResponseEntity entity) throws Exception {
                    int skippedItems = 0;
                    int userId = Integer.parseInt(commonRepository.getUserId());
                    List<Message> messages = new ArrayList<>();
                    for (MessageEntity msg : entity.items) {
                        if ((msg.body == null || msg.body.isEmpty())
                                && (msg.attachment == null || msg.attachment.photo == null)) {
                            ++skippedItems;
                            continue;
                        }

                        MessageAttachment attachment = null;
                        if (msg.attachment != null && msg.attachment.photo != null) {
                            MessageEntityAttachmentPhoto photo = msg.attachment.photo;
                            String photoUriString = getAttachmentPhotoUriString(photo, commonRepository.getUserPhotoSize());
                            Uri photoUri = photoUriString != null ? Uri.parse(photoUriString) : null;
                            attachment = new MessageAttachment(
                                    photoUri,
                                    photo.width,
                                    photo.height
                            );
                        }

                        String timeString = DateUtils.transformDateToHHmmString(new Date(msg.date * 1000));
                        messages.add(new Message(msg.from_id, msg.from_id == userId, msg.body, timeString, attachment));
                    }
                    return new MessageHistoryResponse(entity.count, skippedItems, messages);
                }
            };
        }
        return messageHistoryEntityMapper;
    }


    @Nullable
    private static String getChatPhotoUriString(ChatInfoEntity entity, int userPhotoSize) {
        switch (userPhotoSize) {
            case PhotoSizeUtil.userPhotoSmall: return entity.photo_50;
            case PhotoSizeUtil.userPhotoMedium: return entity.photo_100;
            default: return entity.photo_200;
        }
    }

    @Nullable
    private static String getAttachmentPhotoUriString(MessageEntityAttachmentPhoto photo, int userPhotoSize) {
        String result;
        switch (userPhotoSize) {
            case PhotoSizeUtil.userPhotoBig:
                result = photo.src_xxbig;
                if (result != null) break;

            case PhotoSizeUtil.userPhotoMedium:
                result = photo.src_xbig;
                if (result != null) break;

            default:
                result = photo.src_big;
        }
        return result;
    }

}
