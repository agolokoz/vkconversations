package me.agolokoz.data.repository;

import android.support.annotation.Nullable;

import com.vk.sdk.VKAccessToken;

public class CommonRepository {

    private final int userPhotoSize;

    public CommonRepository(int userPhotoSize) {
        this.userPhotoSize = userPhotoSize;
    }

    @Nullable
    String getUserId() {
        if (VKAccessToken.currentToken() == null) {
            return null;
        }
        return VKAccessToken.currentToken().userId;
    }

    @Nullable
    String getAccessToken() {
        if (VKAccessToken.currentToken() == null) {
            return null;
        }
        return VKAccessToken.currentToken().accessToken;
    }

    public int getUserPhotoSize() {
        return userPhotoSize;
    }
}
