package me.agolokoz.data.repository;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseArray;

import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import me.agolokoz.data.entity.user.UserEntitiesResponse;
import me.agolokoz.data.entity.user.UserEntity;
import me.agolokoz.data.network.UserServiceApi;
import me.agolokoz.data.util.PhotoSizeUtil;
import me.agolokoz.domain.repository.UserRepository;

public class UserRepositoryImpl implements UserRepository {

    @NonNull private final CommonRepository commonRepository;
    @NonNull private final UserServiceApi userServiceApi;
    @NonNull private final SparseArray<Uri> userPhotoUriMap;

    public UserRepositoryImpl(
            @NonNull CommonRepository commonRepository,
            @NonNull UserServiceApi userServiceApi
    ) {
        this.commonRepository = commonRepository;
        this.userServiceApi = userServiceApi;
        this.userPhotoUriMap = new SparseArray<>();
    }

    @Override
    public Observable<SparseArray<Uri>> requestUsersPhotoUri(Set<Integer> userIds) {
        SparseArray<Uri> availableUris = new SparseArray<>();

        StringBuilder userIdsStringBuilder = new StringBuilder();
        for (int userId : userIds) {
            Uri uri = userPhotoUriMap.get(userId, null);
            if (uri == null) {
                userIdsStringBuilder.append(userId).append(",");
            }
            else {
                availableUris.append(userId, uri);
            }
        }

        if (userIdsStringBuilder.length() == 0) {
            return Observable.just(availableUris);
        }
        else {
            userIdsStringBuilder.deleteCharAt(userIdsStringBuilder.length() - 1);
            getUserEntityUriMapMapper().setAvailableUris(availableUris);
            return userServiceApi
                    .requestUserEntities(userIdsStringBuilder.toString(), getPhotoSize(commonRepository.getUserPhotoSize()))
                    .map(getUserEntityUriMapMapper());
        }
    }


    private static String getPhotoSize(int userPhotoSize) {
        switch (userPhotoSize) {
            case PhotoSizeUtil.userPhotoSmall: return "photo_50";
            case PhotoSizeUtil.userPhotoMedium: return "photo_100";
            default: return "photo_200";
        }
    }


    private UserEntityUriMapMapper userEntityUriMapMapper;
    private UserEntityUriMapMapper getUserEntityUriMapMapper() {
        if (userEntityUriMapMapper == null) {
            userEntityUriMapMapper = new UserEntityUriMapMapper();
        }
        return userEntityUriMapMapper;
    }

    private class UserEntityUriMapMapper implements Function<UserEntitiesResponse, SparseArray<Uri>> {
        private SparseArray<Uri> availableUris;

        void setAvailableUris(SparseArray<Uri> availableUris) {
            this.availableUris = availableUris;
        }

        @Override
        public SparseArray<Uri> apply(@NonNull UserEntitiesResponse userEntitiesResponse) throws Exception {
            SparseArray<Uri> result = availableUris.clone();
            for (UserEntity entity : userEntitiesResponse.response) {
                String photoUriString = getUserPhotoUriString(entity, commonRepository.getUserPhotoSize());
                Uri uri = photoUriString != null ? Uri.parse(photoUriString) : null;
                result.put(entity.getId(), uri);
                userPhotoUriMap.put(entity.getId(), uri);
            }
            return result;
        }
    }


    @Nullable
    private static String getUserPhotoUriString(UserEntity entity, int userPhotoSize) {
        switch (userPhotoSize) {
            case PhotoSizeUtil.userPhotoSmall: return entity.getPhoto_50();
            case PhotoSizeUtil.userPhotoMedium: return entity.getPhoto_100();
            default: return entity.getPhoto_200();
        }
    }

}
