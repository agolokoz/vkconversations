package me.agolokoz.data.util;

public class PhotoSizeUtil {

    public static final int userPhotoSmall = 50;
    public static final int userPhotoMedium = 100;
    public static final int userPhotoBig = 200;

    public static int getUserPhotoSizeFromDensity(float density) {
        if (density < 1.5) return userPhotoSmall;
        else if (density < 2.5) return userPhotoMedium;
        return userPhotoBig;
    }

}
