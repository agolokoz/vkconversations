package me.agolokoz.data.entity.dialog;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class DialogsResponseDeserializer implements JsonDeserializer<DialogsResponseEntity> {
    @Override
    public DialogsResponseEntity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = json.getAsJsonObject().getAsJsonArray("response");
        Gson gson = new Gson();

        int totalCount = array.get(0).getAsInt();
        DialogEntity[] entities = new DialogEntity[Math.max(array.size() - 1, 0)];
        for (int i = 1; i != array.size(); ++i) {
            entities[i-1] = gson.fromJson(array.get(i), DialogEntity.class);
        }

        return new DialogsResponseEntity(totalCount, entities);
    }
}
