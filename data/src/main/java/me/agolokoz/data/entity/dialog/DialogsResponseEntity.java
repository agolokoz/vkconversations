package me.agolokoz.data.entity.dialog;

public class DialogsResponseEntity {

    public final int totalDialogsCount;
    public final DialogEntity[] response;

    public DialogsResponseEntity(int count, DialogEntity[] response) {
        this.totalDialogsCount = count;
        this.response = response;
    }
}
