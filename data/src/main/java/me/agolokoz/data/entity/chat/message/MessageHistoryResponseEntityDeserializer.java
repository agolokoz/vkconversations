package me.agolokoz.data.entity.chat.message;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class MessageHistoryResponseEntityDeserializer implements JsonDeserializer<MessageHistoryResponseEntity> {

    @Override
    public MessageHistoryResponseEntity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = json.getAsJsonObject().getAsJsonArray("response");
        int count = array.get(0).getAsInt();
        Gson gson = new Gson();

        int messagesCount = 0;
        for (int i = 1; i != array.size(); ++i) {
            if (array.get(i).isJsonObject()) {
                ++messagesCount;
            }
        }

        int entitiesIndex = 0;
        MessageEntity[] entities = new MessageEntity[messagesCount];
        for (int i = 1; i != array.size(); ++i) {
            if (array.get(i).isJsonObject()) {
                entities[entitiesIndex++] = gson.fromJson(array.get(i), MessageEntity.class);
            }
        }
        return new MessageHistoryResponseEntity(count, entities);
    }
}
