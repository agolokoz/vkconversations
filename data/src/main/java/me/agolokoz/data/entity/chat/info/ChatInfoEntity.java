package me.agolokoz.data.entity.chat.info;

public class ChatInfoEntity {

    public int chat_id;
    public String title;
    public int[] users;
    public String photo_50;
    public String photo_100;
    public String photo_200;
}
