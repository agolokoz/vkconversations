package me.agolokoz.data.entity.user;

public class UserEntity {

    private int uid;
    private String photo_50;
    private String photo_100;
    private String photo_200;
    private String photo_400;

    public int getId() {
        return uid;
    }

    public String getPhoto_50() {
        return photo_50;
    }

    public String getPhoto_100() {
        return photo_100;
    }

    public String getPhoto_200() {
        return photo_200;
    }

    public String getPhoto_400() {
        return photo_400;
    }
}
