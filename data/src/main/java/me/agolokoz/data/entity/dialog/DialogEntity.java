package me.agolokoz.data.entity.dialog;


import java.util.Arrays;
import java.util.Date;

import me.agolokoz.domain.model.chat.info.ChatInfo;
import me.agolokoz.domain.util.DateUtils;

public class DialogEntity {

    public int chat_id = 0;
    public int users_count = 0;
    public long date = 0;
    public String chat_active = null;
    public String title = null;
    public String body = null;

    public static ChatInfo transform(DialogEntity entity) {
        Date date = new Date(entity.date * 1000);
        String lastMessageTime = DateUtils.transformDateToHHmmString(date);

        String[] userStringIds = entity.chat_active.split(",");
        String[] first4UserStringIds = userStringIds.length <= 4
                ? userStringIds
                : Arrays.copyOfRange(userStringIds, 0, 4);

        int[] firstUserIds;
        if (first4UserStringIds.length > 0) {
            firstUserIds = new int[first4UserStringIds.length];
            for (int i = 0; i != first4UserStringIds.length; ++i) {
                try {
                    firstUserIds[i] = Integer.parseInt(first4UserStringIds[i]);
                } catch (NumberFormatException e) {
                    firstUserIds[i] = 0;
                }
            }
        }
        else {
            firstUserIds = new int[0];
        }

        return new ChatInfo(
                entity.chat_id,
                entity.title,
                entity.body,
                lastMessageTime,
                firstUserIds,
                entity.users_count
        );
    }

}
