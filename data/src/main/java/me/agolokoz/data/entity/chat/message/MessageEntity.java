package me.agolokoz.data.entity.chat.message;

import me.agolokoz.data.entity.chat.message.attachment.MessageEntityAttachment;

public class MessageEntity {

    public int from_id;
    public long date;
    public String body;
    public MessageEntityAttachment attachment;

}
