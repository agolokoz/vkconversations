package me.agolokoz.data.entity.chat.message.attachment;

public class MessageEntityAttachmentPhoto {

    public String src;
    public String src_big;
    public String src_small;
    public String src_xbig;
    public String src_xxbig;
    public int width;
    public int height;
}