package me.agolokoz.data.entity.chat.message;

public class MessageHistoryResponseEntity {

    public int count;
    public MessageEntity[] items;

    public MessageHistoryResponseEntity(int count, MessageEntity[] items) {
        this.count = count;
        this.items = items;
    }

}
