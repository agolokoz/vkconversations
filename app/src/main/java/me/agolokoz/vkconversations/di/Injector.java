package me.agolokoz.vkconversations.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import me.agolokoz.data.entity.chat.message.MessageHistoryResponseEntity;
import me.agolokoz.data.entity.chat.message.MessageHistoryResponseEntityDeserializer;
import me.agolokoz.data.entity.dialog.DialogsResponseDeserializer;
import me.agolokoz.data.entity.dialog.DialogsResponseEntity;
import me.agolokoz.data.network.MessagesServiceApi;
import me.agolokoz.data.network.UserServiceApi;
import me.agolokoz.data.repository.CommonRepository;
import me.agolokoz.data.repository.MessagesRepositoryImpl;
import me.agolokoz.data.repository.UserRepositoryImpl;
import me.agolokoz.data.util.PhotoSizeUtil;
import me.agolokoz.domain.repository.MessagesRepository;
import me.agolokoz.domain.repository.UserRepository;
import me.agolokoz.domain.util.GLog;
import me.agolokoz.vkconversations.App;
import me.agolokoz.vkconversations.R;
import me.agolokoz.vkconversations.util.scheduler.SchedulerProvider;
import me.agolokoz.vkconversations.util.scheduler.SchedulerProviderImpl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Injector {

    private static String host;
    private static float density;

    private static Retrofit retrofit;
    private static SchedulerProvider schedulerProvider;

    private static CommonRepository commonRepository;
    private static MessagesRepository messagesRepository;
    private static UserRepository userRepository;

    public static void init(Context appContext) {
        host = appContext.getString(R.string.host);
        density = appContext.getResources().getDisplayMetrics().density;
    }

    private static CommonRepository getCommonRepository()  {
        if (commonRepository == null) {
            commonRepository = new CommonRepository(PhotoSizeUtil.getUserPhotoSizeFromDensity(density));
        }
        return commonRepository;
    }


    private static Retrofit getRetrofit() {
        if (retrofit == null) {
            Interceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                @Override
                public void log(@NonNull String message) {
                    GLog.d(message);
                }
            }).setLevel(HttpLoggingInterceptor.Level.BODY);

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(DialogsResponseEntity.class, new DialogsResponseDeserializer())
                    .registerTypeAdapter(MessageHistoryResponseEntity.class, new MessageHistoryResponseEntityDeserializer())
                    .create();

            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(loggingInterceptor)
                    .build();

            retrofit = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl(host)
                    .client(httpClient)
                    .build();
        }
        return retrofit;
    }


    public static MessagesRepository getMessagesRepository() {
        if (messagesRepository == null) {
            MessagesServiceApi messagesServiceApi = getRetrofit().create(MessagesServiceApi.class);
            messagesRepository = new MessagesRepositoryImpl(getCommonRepository(), messagesServiceApi);
        }
        return messagesRepository;
    }


    public static UserRepository getUserRepository() {
        if (userRepository == null) {
            UserServiceApi userServiceApi = getRetrofit().create(UserServiceApi.class);
            userRepository = new UserRepositoryImpl(Injector.getCommonRepository(), userServiceApi);
        }
        return userRepository;
    }


    public static SchedulerProvider getSchedulerProvider() {
        if (schedulerProvider == null) {
            schedulerProvider = new SchedulerProviderImpl();
        }
        return schedulerProvider;
    }

}
