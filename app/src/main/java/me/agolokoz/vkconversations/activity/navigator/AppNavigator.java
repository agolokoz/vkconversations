package me.agolokoz.vkconversations.activity.navigator;

public interface AppNavigator {

    void showConversationsListScreen();
    void showChatScreen(int conversationId);
}
