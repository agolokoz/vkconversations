package me.agolokoz.vkconversations.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import me.agolokoz.vkconversations.R;
import me.agolokoz.vkconversations.activity.navigator.AppNavigator;
import me.agolokoz.vkconversations.activity.navigator.AppNavigatorImpl;

public class MainActivity extends AppCompatActivity {

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    private AppNavigator appNavigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appNavigator = new AppNavigatorImpl(getSupportFragmentManager(), R.id.main_container_layout);

        if (savedInstanceState == null) {
            if (VKSdk.isLoggedIn()) {
                appNavigator.showConversationsListScreen();
            } else {
                VKSdk.login(this, "messages");
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (VKSdk.isLoggedIn()) {
            appNavigator.showConversationsListScreen();
        }
    }

    public AppNavigator getAppNavigator() {
        return appNavigator;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, loginCallback)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }
        else {
            getFragmentManager().popBackStack();
        }
    }


    private final VKCallback<VKAccessToken> loginCallback = new VKCallback<VKAccessToken>() {
        @Override
        public void onResult(VKAccessToken res) {
            MainActivity.startActivity(MainActivity.this);
        }

        @Override
        public void onError(VKError error) {
            Toast.makeText(getBaseContext(), error.toString(), Toast.LENGTH_LONG).show();
        }
    };
}
