package me.agolokoz.vkconversations.activity.navigator;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import me.agolokoz.vkconversations.screen.chat.ChatFragment;
import me.agolokoz.vkconversations.screen.list.ConversationsListFragment;

public class AppNavigatorImpl implements AppNavigator {

    @NonNull private final FragmentManager fragmentManager;
    private final int layoutResId;

    public AppNavigatorImpl(@NonNull FragmentManager fragmentManager, int layoutResId) {
        this.fragmentManager = fragmentManager;
        this.layoutResId = layoutResId;
    }

    @Override
    public void showConversationsListScreen() {
        ConversationsListFragment fragment = (ConversationsListFragment) fragmentManager.findFragmentByTag(ConversationsListFragment.class.getSimpleName());
        if (fragment == null) {
            fragment = ConversationsListFragment.createInstance();
            fragmentManager.beginTransaction()
                    .add(layoutResId, fragment, ConversationsListFragment.class.getSimpleName())
                    .commit();
        }
    }

    @Override
    public void showChatScreen(int conversationId) {
        ChatFragment fragment = ChatFragment.createInstance(conversationId);
        fragmentManager.beginTransaction()
                .add(layoutResId, fragment, ChatFragment.class.getSimpleName())
                .addToBackStack(null)
                .commit();
    }
}
