package me.agolokoz.vkconversations.util;

import android.support.annotation.NonNull;

import io.reactivex.functions.Consumer;
import me.agolokoz.vkconversations.BasePresenter;
import me.agolokoz.vkconversations.BaseView;
import me.agolokoz.vkconversations.util.scheduler.SchedulerProvider;

public abstract class AbstractPresenter extends RxObservablesHandler implements BasePresenter {

    private final BaseView<?> view;

    public AbstractPresenter(
            @NonNull SchedulerProvider schedulerProvider,
            @NonNull BaseView<?> view
    ) {
        super(schedulerProvider);
        this.view = view;
    }

    public final Consumer<Throwable> errorConsumer = new Consumer<Throwable>() {
        @Override
        public void accept(Throwable throwable) throws Exception {
            view.showMessage(throwable.getMessage());
        }
    };

}
