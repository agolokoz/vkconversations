package me.agolokoz.vkconversations.util;

public interface AdapterItemClickListener {

    void onItemClicked(int position);
}
