package me.agolokoz.vkconversations.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

import me.agolokoz.data.util.PhotoSizeUtil;
import me.agolokoz.vkconversations.R;

public class BitmapUtil {

    private static Bitmap deactivatedBitmap;
    public static Bitmap getDeactivatedBitmap(@NonNull Context context) {
        if (deactivatedBitmap == null) {
            deactivatedBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.deactivated_200);
        }
        return deactivatedBitmap;
    }

    private static Bitmap emptyBitmap;
    public static Bitmap getEmptyBitmap(@NonNull Context context) {
        if (emptyBitmap == null) {
            int size = PhotoSizeUtil.getUserPhotoSizeFromDensity(context.getResources().getDisplayMetrics().density);
            emptyBitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        }
        return emptyBitmap;
    }

    public static Bitmap getCompositeBitmap(@NonNull Context context, Bitmap[] bitmaps, int size) {
        Bitmap bitmap = null;
        try {
            bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);

            int bitmapsCount = bitmaps.length;
            switch (bitmapsCount) {
                case 2: {
                    bitmaps[0] = getCenterCroppedVerticalBitmap(bitmaps[0]);
                    bitmaps[1] = getCenterCroppedVerticalBitmap(bitmaps[1]);
                    break;
                }
                case 3: {
                    bitmaps[0] = getCenterCroppedVerticalBitmap(bitmaps[0]);
                    break;
                }
            }

            Drawable[] drawables = new Drawable[bitmapsCount];
            for (int i = 0; i != bitmapsCount; ++i) {
                drawables[i] = new BitmapDrawable(context.getResources(), bitmaps[i]);
            }

            int halfSize = size / 2;
            int space = (int) (1 * context.getResources().getDisplayMetrics().density);
            switch (bitmapsCount) {
                case 2: {
                    drawables[0].setBounds(0, 0, halfSize - space, size);
                    drawables[1].setBounds(halfSize + space, 0, size, size);
                    break;
                }

                case 3: {
                    drawables[0].setBounds(0, 0, halfSize - space, size);
                    drawables[1].setBounds(halfSize + space, 0, size, halfSize - space);
                    drawables[2].setBounds(halfSize + space, halfSize + space, size, size);
                    break;
                }

                case 4: {
                    drawables[0].setBounds(0, 0, halfSize - space, halfSize - space);
                    drawables[1].setBounds(0, halfSize + space, halfSize - space, size);
                    drawables[2].setBounds(halfSize + space, 0, size, halfSize - space);
                    drawables[3].setBounds(halfSize + space, halfSize + space, size, size);
                    break;
                }
            }

            for (int i = 0; i != bitmapsCount; ++i) {
                drawables[i].draw(canvas);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private static Bitmap getCenterCroppedVerticalBitmap(Bitmap original) {
        int halfWidth = original.getWidth() / 2;
        int cropWidth = halfWidth / 2;
        return Bitmap.createBitmap(original, cropWidth, 0, halfWidth, original.getHeight());
    }
    
}
