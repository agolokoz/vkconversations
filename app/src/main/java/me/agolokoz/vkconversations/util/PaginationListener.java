package me.agolokoz.vkconversations.util;

public interface PaginationListener {

    void onPagination(int offset);
}