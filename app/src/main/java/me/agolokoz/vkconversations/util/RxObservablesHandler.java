package me.agolokoz.vkconversations.util;

import android.support.annotation.NonNull;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import me.agolokoz.vkconversations.util.scheduler.SchedulerProvider;

public abstract class RxObservablesHandler {

    @NonNull private final CompositeDisposable compositeDisposable;
    @NonNull private final SchedulerProvider schedulerProvider;

    public RxObservablesHandler(@NonNull SchedulerProvider schedulerProvider) {
        this.compositeDisposable = new CompositeDisposable();
        this.schedulerProvider = schedulerProvider;
    }

    public <T> Observable<T> prepareObservable(Observable<T> observable) {
        return observable
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui());
    }

    public void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    protected void clearDisposables() {
        compositeDisposable.clear();
    }

}
