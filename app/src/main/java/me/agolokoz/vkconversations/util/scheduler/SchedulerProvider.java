package me.agolokoz.vkconversations.util.scheduler;

import android.support.annotation.NonNull;

import io.reactivex.Scheduler;

public interface SchedulerProvider {

    @NonNull
    Scheduler io();

    @NonNull
    Scheduler ui();

}
