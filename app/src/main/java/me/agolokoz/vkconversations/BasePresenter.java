package me.agolokoz.vkconversations;

public interface BasePresenter {

    void start();
    void stop();
}
