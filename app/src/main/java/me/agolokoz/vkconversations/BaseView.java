package me.agolokoz.vkconversations;

public interface BaseView<P extends BasePresenter> {

    void showMessage(String message);
    void setPresenter(P presenter);
}
