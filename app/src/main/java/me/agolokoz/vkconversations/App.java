package me.agolokoz.vkconversations;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.vk.sdk.VKSdk;

import me.agolokoz.vkconversations.di.Injector;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Injector.init(this);
        VKSdk.initialize(this);
        Fresco.initialize(this);
    }
}
