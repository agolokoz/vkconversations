package me.agolokoz.vkconversations.screen.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import me.agolokoz.domain.model.chat.info.ChatInfo;
import me.agolokoz.vkconversations.R;
import me.agolokoz.vkconversations.util.AdapterItemClickListener;

public class ConversationsAdapter extends RecyclerView.Adapter<ConversationsAdapter.ConversationViewHolder> {

    private final AdapterItemClickListener clickListener;
    private final List<ChatInfo> chatInfoList = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public ConversationsAdapter(AdapterItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void addData(List<ChatInfo> newChatInfos) {
        chatInfoList.addAll(newChatInfos);
        notifyItemRangeInserted(this.chatInfoList.size(), newChatInfos.size());
    }

    public void updateItem(int position) {
        notifyItemChanged(position);
    }

    @Override
    public ConversationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        View view = layoutInflater.inflate(R.layout.item_conversation, parent, false);
        return new ConversationViewHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(ConversationViewHolder holder, int position) {
        holder.update(chatInfoList.get(position));
    }

    @Override
    public int getItemCount() {
        return chatInfoList.size();
    }

    static class ConversationViewHolder extends RecyclerView.ViewHolder {

        private AdapterItemClickListener itemClickListener;
        private final TextView titleTextView;
        private final TextView messageTextView;
        private final TextView timeTextView;
        private final SimpleDraweeView imageView;

        ConversationViewHolder(View itemView, AdapterItemClickListener adapterItemClickListener) {
            super(itemView);
            this.itemClickListener = adapterItemClickListener;

            titleTextView = itemView.findViewById(R.id.conversationTitleTextView);
            messageTextView = itemView.findViewById(R.id.conversationMessageTextView);
            timeTextView = itemView.findViewById(R.id.conversationTimeTextView);
            imageView = itemView.findViewById(R.id.conversationImageView);

            itemView.setOnClickListener(clickListener);
        }

        void update(@NonNull ChatInfo chatInfo) {
            titleTextView.setText(chatInfo.getTitle());
            messageTextView.setText(chatInfo.getLastMessage());
            timeTextView.setText(chatInfo.getLastMessageTime());

            if (chatInfo.getPhotoUri() != null) {
                imageView.setImageURI(chatInfo.getPhotoUri());
            }
            else if (chatInfo.getDrawable() != null) {
                imageView.setImageDrawable(chatInfo.getDrawable());
            }
            else {
                imageView.setImageResource(android.R.color.transparent);
            }
        }

        private final View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClicked(getAdapterPosition());
            }
        };
    }

}
