package me.agolokoz.vkconversations.screen.chat;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import me.agolokoz.domain.model.chat.info.ChatInfo;
import me.agolokoz.domain.model.chat.message.Message;
import me.agolokoz.domain.model.chat.message.MessageHistoryResponse;
import me.agolokoz.domain.repository.MessagesRepository;
import me.agolokoz.domain.repository.UserRepository;
import me.agolokoz.vkconversations.screen.chat.handler.ChatCompositePhotoHandler;
import me.agolokoz.vkconversations.screen.chat.handler.ChatUserPhotoHandler;
import me.agolokoz.vkconversations.util.AbstractPresenter;
import me.agolokoz.vkconversations.util.scheduler.SchedulerProvider;

public class ChatPresenter extends AbstractPresenter implements ChatContract.Presenter {

    @NonNull private final ChatContract.View view;
    @NonNull private final MessagesRepository messagesRepository;
    @NonNull private final List<Message> historyList;
    @NonNull private final ChatCompositePhotoHandler compositePhotoHandler;
    @NonNull private final ChatUserPhotoHandler userPhotoHandler;

    private final int chatId;
    private int skippedItemsCount;

    public ChatPresenter(
            @NonNull MessagesRepository messagesRepository,
            @NonNull UserRepository userRepository,
            @NonNull SchedulerProvider schedulerProvider,
            @NonNull ChatContract.View view,
            @NonNull Context context,
            int chatId
    ) {
        super(schedulerProvider, view);
        this.messagesRepository = messagesRepository;
        this.view = view;
        this.chatId = chatId;
        this.historyList = new ArrayList<>();
        this.compositePhotoHandler = new ChatCompositePhotoHandler(this, userRepository, this, context);
        this.userPhotoHandler = new ChatUserPhotoHandler(this, userRepository, this, historyList);
    }

    @Override
    public void start() {
        historyList.clear();
        skippedItemsCount = 0;
        view.clearHistoryItems();
        loadChatInfo();
        loadHistory(0);
    }

    @Override
    public void stop() {}

    private void loadChatInfo() {
        Disposable disposable = prepareObservable(messagesRepository.requestChatInfo(chatId))
                .subscribe(getChatInfoConsumer(), errorConsumer);
        addDisposable(disposable);
    }

    @Override
    public void loadHistory(final int offset) {
        if (offset == 0) {
            view.setLoadingState();
        }

        Disposable disposable = prepareObservable(messagesRepository.requestChatHistory(chatId, offset + skippedItemsCount))
                .subscribe(getMessageHistoryResponseConsumer(), errorConsumer);
        addDisposable(disposable);
    }

    @Override
    public void updateItem(int position) {
        view.updateItem(position);
    }

    @Override
    public void updateChatImage(Drawable drawable) {
        view.setConversationDrawable(drawable);
    }


    private Consumer<ChatInfo> chatInfoConsumer;
    private Consumer<ChatInfo> getChatInfoConsumer() {
        if (chatInfoConsumer == null) {
            chatInfoConsumer = new Consumer<ChatInfo>() {
                @Override
                public void accept(ChatInfo chatInfo) throws Exception {
                    view.setConversationInfo(chatInfo.getTitle(), chatInfo.getParticipantsCount(), chatInfo.getPhotoUri());
                    if (chatInfo.getPhotoUri() == null) {
                        compositePhotoHandler.loadCompositePhoto(chatInfo.getFirstUserIds());
                    }
                }
            };
        }
        return chatInfoConsumer;
    }

    private Consumer<MessageHistoryResponse> messageHistoryResponseConsumer;
    private Consumer<MessageHistoryResponse> getMessageHistoryResponseConsumer() {
        if (messageHistoryResponseConsumer == null) {
            messageHistoryResponseConsumer = new Consumer<MessageHistoryResponse>() {
                @Override
                public void accept(MessageHistoryResponse historyResponse) throws Exception {
                    view.setLoadedState();
                    skippedItemsCount += historyResponse.getItemsSkipped();
                    historyList.addAll(historyResponse.getItems());
                    view.addHistoryItems(historyResponse.getItems(), historyResponse.getCount() - skippedItemsCount);
                    userPhotoHandler.handle(historyResponse.getItems());
                }
            };
        }
        return messageHistoryResponseConsumer;
    }

}
