package me.agolokoz.vkconversations.screen.chat.adapter;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import me.agolokoz.domain.model.chat.message.Message;
import me.agolokoz.domain.model.chat.message.attachment.MessageAttachment;
import me.agolokoz.vkconversations.R;

class ChatItemViewHolder extends RecyclerView.ViewHolder {
    private static final String timeTextStub = "88:88";

    private static int topBottomItemSameSpace;
    private static int topBottomItemDiffSpace;
    private static int topBottomItemBoundSpace;
    private static int contentLayoutSpace;
    private static int participantAvatarImageWidth;
    private static int maxParticipantAttachmentImageWidth;
    private static int maxUserAttachmentImageWidth;

    private ViewGroup participantLayout;
    private ViewGroup participantContentLayout;
    private TextView participantTextView;
    private TextView participantTimeTextView;
    private ImageView participantAvatarImageView;
    private SimpleDraweeView participantAttachmentImageView;
    private View participantTailImage;

    private ViewGroup userLayout;
    private ViewGroup userContentLayout;
    private TextView userTextView;
    private TextView userTimeTextView;
    private SimpleDraweeView userAttachmentImageView;
    private View userTailImage;

    ChatItemViewHolder(View itemView) {
        super(itemView);
        if (topBottomItemSameSpace == 0)  { topBottomItemSameSpace  = itemView.getContext().getResources().getDimensionPixelSize(R.dimen.message_item_same_top_bot_space);  }
        if (topBottomItemDiffSpace == 0)  { topBottomItemDiffSpace  = itemView.getContext().getResources().getDimensionPixelSize(R.dimen.message_item_diff_top_bot_space);  }
        if (topBottomItemBoundSpace == 0) { topBottomItemBoundSpace = itemView.getContext().getResources().getDimensionPixelSize(R.dimen.message_item_bound_top_bot_space); }
        if (contentLayoutSpace == 0)      { contentLayoutSpace = itemView.getContext().getResources().getDimensionPixelSize(R.dimen.message_item_content_space); }

        participantLayout = itemView.findViewById(R.id.messageFromParticipantLayout);
        participantContentLayout = itemView.findViewById(R.id.messageParticipantContent);
        participantTextView = itemView.findViewById(R.id.messageParticipantText);
        participantTimeTextView = itemView.findViewById(R.id.messageParticipantTimeText);
        participantAvatarImageView = itemView.findViewById(R.id.messageParticipantAvatarImage);
        participantAttachmentImageView = itemView.findViewById(R.id.messageParticipantAttachmentImage);
        participantTailImage = itemView.findViewById(R.id.messageParticipantTailImage);
        if (participantAvatarImageWidth == 0) {
            participantAvatarImageWidth = participantAvatarImageView.getLayoutParams().width;
        }

        if (maxParticipantAttachmentImageWidth == 0) {
            int displayWidth = itemView.getContext().getResources().getDisplayMetrics().widthPixels;
            FrameLayout.LayoutParams contentLayoutParams = (FrameLayout.LayoutParams) participantLayout.getLayoutParams();
            LinearLayout.LayoutParams timeLayoutParams = (LinearLayout.LayoutParams) participantTimeTextView.getLayoutParams();
            LinearLayout.LayoutParams tailLayoutParams = (LinearLayout.LayoutParams) participantTailImage.getLayoutParams();

            participantTimeTextView.setText(timeTextStub);
            participantTimeTextView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            int timeTextWidth = participantTimeTextView.getMeasuredWidth();

            maxParticipantAttachmentImageWidth = displayWidth
                    - (participantLayout.getPaddingLeft() + participantLayout.getPaddingRight())
                    - (participantAvatarImageWidth)
                    - (tailLayoutParams.width + contentLayoutParams.rightMargin)
                    - (participantAttachmentImageView.getPaddingLeft() + participantAttachmentImageView.getPaddingLeft())
                    - (timeLayoutParams.leftMargin + timeLayoutParams.rightMargin)
                    - timeTextWidth;
        }

        userLayout = itemView.findViewById(R.id.messageFromUserLayout);
        userContentLayout = itemView.findViewById(R.id.messageUserContentLayout);
        userTextView = itemView.findViewById(R.id.messageUserText);
        userTimeTextView = itemView.findViewById(R.id.messageUserTimeText);
        userAttachmentImageView = itemView.findViewById(R.id.messageUserAttachmentImage);
        userTailImage = itemView.findViewById(R.id.messageUserTailImage);
        if (maxUserAttachmentImageWidth == 0) {
            int displayWidth = itemView.getContext().getResources().getDisplayMetrics().widthPixels;
            FrameLayout.LayoutParams contentLayoutParams = (FrameLayout.LayoutParams) userLayout.getLayoutParams();
            LinearLayout.LayoutParams timeLayoutParams = (LinearLayout.LayoutParams) userTimeTextView.getLayoutParams();

            userTimeTextView.setText(timeTextStub);
            userTimeTextView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            int timeTextWidth = userTimeTextView.getMeasuredWidth();

            maxUserAttachmentImageWidth = displayWidth
                    - (userLayout.getPaddingLeft() + userLayout.getPaddingRight())
                    - (contentLayoutParams.leftMargin + contentLayoutParams.rightMargin)
                    - (userAttachmentImageView.getPaddingLeft() + userAttachmentImageView.getPaddingLeft())
                    - (timeLayoutParams.leftMargin + timeLayoutParams.rightMargin)
                    - timeTextWidth;
        }
    }

    void update(Message message, boolean isPrevFromThisUser, boolean isNextFromThisUser, boolean isLastItem) {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        layoutParams.topMargin = isNextFromThisUser ? topBottomItemSameSpace : topBottomItemDiffSpace;
        layoutParams.bottomMargin = isPrevFromThisUser ? topBottomItemSameSpace : topBottomItemDiffSpace;
        if (getAdapterPosition() == 0) { layoutParams.bottomMargin = topBottomItemBoundSpace; }
        if (isLastItem) { layoutParams.topMargin = topBottomItemBoundSpace; }
        itemView.setLayoutParams(layoutParams);

        if (message.isFromUser()) {
            participantLayout.setVisibility(View.GONE);

            // Tail
            boolean tail = !isNextFromThisUser;
            userTailImage.setVisibility(tail ? View.VISIBLE : View.GONE);
            userContentLayout.setBackgroundResource(tail ? R.drawable.message_from_user_tail : R.drawable.message_from_user);
            LinearLayout.LayoutParams contentLayoutParams = (LinearLayout.LayoutParams) userContentLayout.getLayoutParams();
            contentLayoutParams.rightMargin = tail ? 0 : contentLayoutSpace;
            userContentLayout.setLayoutParams(contentLayoutParams);

            updateItem(message, userLayout, userTextView, userTimeTextView, userAttachmentImageView, maxUserAttachmentImageWidth);
        }
        else {
            userLayout.setVisibility(View.GONE);

            // Tail
            boolean tail = !isNextFromThisUser;
            participantTailImage.setVisibility(tail ? View.VISIBLE : View.GONE);
            participantAvatarImageView.setVisibility(tail ? View.VISIBLE : View.GONE);
            participantContentLayout.setBackgroundResource(tail ? R.drawable.message_from_participant_tail : R.drawable.message_from_participant);
            LinearLayout.LayoutParams contentLayoutParams = (LinearLayout.LayoutParams) participantContentLayout.getLayoutParams();
            contentLayoutParams.leftMargin = tail ? 0 : contentLayoutSpace + participantAvatarImageWidth;
            participantContentLayout.setLayoutParams(contentLayoutParams);

            updateItem(message, participantLayout, participantTextView, participantTimeTextView, participantAttachmentImageView, maxParticipantAttachmentImageWidth);
            if (message.getSenderPhotoUri() != null && tail) {
                participantAvatarImageView.setImageURI(message.getSenderPhotoUri());
            }
        }
    }

    private void updateItem(
            Message message,
            ViewGroup messageLayout,
            TextView messageTextView,
            TextView messageTimeTextView,
            SimpleDraweeView attachmentImageView,
            int maxAttachmentImageWidth)
    {
        messageLayout.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            messageTextView.setText(Html.fromHtml(message.getText(), Html.FROM_HTML_MODE_LEGACY));
        }
        else {
            messageTextView.setText(Html.fromHtml(message.getText()));
        }
        messageTextView.setVisibility(message.getText() != null && !message.getText().isEmpty() ? View.VISIBLE : View.GONE);
        messageTimeTextView.setText(message.getTimeString());

        MessageAttachment attachment = message.getAttachment();
        if (attachment != null) {
            attachmentImageView.setImageURI(attachment.getPhotoUri());
            attachmentImageView.setVisibility(View.VISIBLE);
            updateAttachmentImage(attachment, attachmentImageView, maxAttachmentImageWidth);
        }
        else {
            attachmentImageView.setVisibility(View.GONE);
        }
    }

    private void updateAttachmentImage(MessageAttachment attachment, SimpleDraweeView attachmentImageView, int maxAttachmentImageWidth) {
        int imageHeight = attachment.getHeight();
        int imageWidth = attachment.getWidth();

        if (imageWidth > maxAttachmentImageWidth) {
            float ratio = imageHeight * 1.0f / imageWidth;
            imageWidth = maxAttachmentImageWidth;
            imageHeight = (int)(imageWidth * ratio);
        }

        LinearLayout.LayoutParams imgLP = (LinearLayout.LayoutParams) attachmentImageView.getLayoutParams();
        imgLP.width = imageWidth;
        imgLP.height = imageHeight;
        attachmentImageView.setLayoutParams(imgLP);
    }
}