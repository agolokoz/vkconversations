package me.agolokoz.vkconversations.screen.list;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import me.agolokoz.domain.model.chat.info.ChatInfo;
import me.agolokoz.domain.model.chat.info.ChatsResponse;
import me.agolokoz.domain.repository.MessagesRepository;
import me.agolokoz.domain.repository.UserRepository;
import me.agolokoz.vkconversations.activity.navigator.AppNavigator;
import me.agolokoz.vkconversations.util.AbstractPresenter;
import me.agolokoz.vkconversations.util.scheduler.SchedulerProvider;

public class ConversationListPresenter extends AbstractPresenter implements ConversationsListContract.Presenter {

    private static final int maxConversationsCount = 10;

    @NonNull private final MessagesRepository messagesRepository;
    @NonNull private final ConversationsListContract.View view;
    @NonNull private final ConversationPhotoHandler compositePhotosHandler;
    @NonNull private final List<ChatInfo> chatInfos;
    private int skippedItemsCount = 0;

    private AppNavigator appNavigator;

    ConversationListPresenter(
            @NonNull MessagesRepository messagesRepository,
            @NonNull UserRepository userRepository,
            @NonNull SchedulerProvider schedulerProvider,
            @NonNull ConversationsListContract.View view,
            @NonNull Context context
    ) {
        super(schedulerProvider, view);
        this.messagesRepository = messagesRepository;
        this.view = view;
        this.view.setPresenter(this);
        this.chatInfos = new ArrayList<>();
        this.compositePhotosHandler = new ConversationPhotoHandler(this, userRepository, this, this.chatInfos, context);
    }

    @Override
    public void start() {
        loadConversations();
    }

    @Override
    public void stop() {
        clearDisposables();
    }

    @Override
    public void setAppNavigator(AppNavigator appNavigator) {
        this.appNavigator = appNavigator;
    }

    @Override
    public void loadConversations() {
        chatInfos.clear();
        loadConversations(0);
    }

    private void loadConversations(int offset) {
        getConversationsConsumer().setOffset(offset);
        Disposable disposable =
                prepareObservable(messagesRepository.requestConversations(offset))
                        .subscribe(getConversationsConsumer(), errorConsumer);
        addDisposable(disposable);
    }

    private void onAllConversationsLoaded() {
        int[] conversationsIds = new int[chatInfos.size()];
        for (int i = 0; i != conversationsIds.length; ++i) {
            conversationsIds[i] = chatInfos.get(i).getId();
        }

        Disposable disposable = prepareObservable(messagesRepository.requestConversationsPhotoUri(conversationsIds))
                .subscribe(getUriConsumer(), errorConsumer);
        addDisposable(disposable);
    }

    @Override
    public void conversationChanged(int position) {
        view.updateConversation(position);
    }

    @Override
    public void onConversationItemClicked(int position) {
        if (appNavigator == null) {
            return;
        }

        ChatInfo chatInfo = chatInfos.get(position);
        appNavigator.showChatScreen(chatInfo.getId());
    }


    private Consumer<SparseArray<Uri>> uriConsumer;
    private Consumer<SparseArray<Uri>> getUriConsumer() {
        if (uriConsumer == null) {
            uriConsumer =
                    new Consumer<SparseArray<Uri>>() {
                        @Override
                        public void accept(SparseArray<Uri> uriMap) throws Exception {
                            for (int i = 0; i != chatInfos.size(); ++i) {
                                ChatInfo chatInfo = chatInfos.get(i);
                                Uri photoUri = uriMap.get(chatInfo.getId(), null);
                                if (photoUri != null) {
                                    chatInfo.setPhotoUri(photoUri);
                                    view.updateConversation(i);
                                }
                            }
                            compositePhotosHandler.requestCompositePhotos();
                        }
                    };
        }
        return uriConsumer;
    }

    private ConversationsConsumer conversationsConsumer;
    private ConversationsConsumer getConversationsConsumer() {
        if (conversationsConsumer == null) {
            conversationsConsumer = new ConversationsConsumer();
        }
        return conversationsConsumer;
    }

    private class ConversationsConsumer implements Consumer<ChatsResponse> {
        private int offset = 0;

        void setOffset(int offset) {
            this.offset = offset;
        }

        @Override
        public void accept(ChatsResponse chatsResponse) throws Exception {
            skippedItemsCount += chatsResponse.getSkipped();

            List<ChatInfo> newChatInfos = chatsResponse.getChatInfoList();
            if (chatInfos.size() + newChatInfos.size() > maxConversationsCount) {
                int count = maxConversationsCount - chatInfos.size();
                newChatInfos = newChatInfos.subList(0, count);
            }

            chatInfos.addAll(newChatInfos);
            view.addConversations(newChatInfos);
            if (chatInfos.size() < maxConversationsCount && offset + skippedItemsCount < chatsResponse.getTotal()) {
                loadConversations(offset + skippedItemsCount);
            }
            else {
                onAllConversationsLoaded();
            }
        }
    }

}
