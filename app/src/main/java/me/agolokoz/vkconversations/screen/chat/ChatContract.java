package me.agolokoz.vkconversations.screen.chat;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;

import java.util.List;

import me.agolokoz.domain.model.chat.message.Message;
import me.agolokoz.vkconversations.BasePresenter;
import me.agolokoz.vkconversations.BaseView;

public interface ChatContract {

    interface Presenter extends BasePresenter {

        void loadHistory(int offset);
        void updateItem(int position);
        void updateChatImage(Drawable drawable);
    }

    interface View extends BaseView<Presenter> {

        void setLoadedState();
        void setLoadingState();

        void addHistoryItems(List<Message> items, int totalItemsCount);
        void clearHistoryItems();
        void updateItem(int position);
        void setConversationInfo(String title, int participantsCount, @Nullable Uri photoUri);
        void setConversationDrawable(Drawable drawable);
    }

}
