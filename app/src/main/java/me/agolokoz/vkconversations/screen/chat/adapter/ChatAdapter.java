package me.agolokoz.vkconversations.screen.chat.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import me.agolokoz.domain.model.chat.message.Message;
import me.agolokoz.vkconversations.R;
import me.agolokoz.vkconversations.util.PaginationListener;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int viewTypeDefault = 0;
    private static final int viewTypeLoading = 1;

    private final List<Message> historyList = new ArrayList<>();
    private final PaginationListener paginationListener;

    private LayoutInflater layoutInflater;
    private int totalItemsCount;
    private boolean waitForData;
    private boolean isPaginable;

    public ChatAdapter(PaginationListener paginationListener) {
        this.paginationListener = paginationListener;
        clear();
    }

    public void addData(List<Message> histories, int totalItemsCount) {
        this.totalItemsCount = totalItemsCount;
        historyList.addAll(histories);
        notifyItemRangeInserted(getItemCount(), getItemCount() + histories.size() - 1);
        waitForData = false;
    }

    public void clear() {
        this.historyList.clear();
        this.totalItemsCount = 0;
        this.waitForData = true;
        this.isPaginable = false;
        notifyDataSetChanged();
    }

    public void updateItem(int position) {
        notifyItemChanged(position);
    }

    public void setPaginable(boolean paginable) {
        isPaginable = paginable;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        switch (viewType) {
            case viewTypeLoading: {
                View view = layoutInflater.inflate(R.layout.item_loading, parent, false);
                return new LoadingViewHolder(view);
            }

            default: {
                View view = layoutInflater.inflate(R.layout.item_message, parent, false);
                return new ChatItemViewHolder(view);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (!waitForData && isNeedToLoadData(position)) {
            waitForData = true;
            paginationListener.onPagination(getDataCount());
        }

        if (position < getDataCount()-1 || (position == getDataCount()-1 && getDataCount() == totalItemsCount)) {
            boolean isPrevFromThisUser = (position > 0 && historyList.get(position - 1).getSenderId() == historyList.get(position).getSenderId());
            boolean isNextFromThisUser = (position < historyList.size() - 1 && historyList.get(position).getSenderId() == historyList.get(position + 1).getSenderId());
            ((ChatItemViewHolder) holder).update(historyList.get(position), isPrevFromThisUser, isNextFromThisUser, position == getItemCount()-1);
        }
    }

    @Override
    public int getItemCount() {
        return historyList.size() + ((historyList.size() < totalItemsCount) && isPaginable ? 1 : 0);
    }

    public int getDataCount() {
        return historyList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == historyList.size() && isPaginable) ? viewTypeLoading : viewTypeDefault;
    }

    private boolean isNeedToLoadData(int position) {
        int itemCount = historyList.size();
        return isPaginable
                && position >= itemCount - 3
                && 0 < itemCount && itemCount < totalItemsCount;
    }

}
