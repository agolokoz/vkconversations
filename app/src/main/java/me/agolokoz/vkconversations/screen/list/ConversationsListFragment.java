package me.agolokoz.vkconversations.screen.list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import me.agolokoz.domain.model.chat.info.ChatInfo;
import me.agolokoz.vkconversations.R;
import me.agolokoz.vkconversations.activity.MainActivity;
import me.agolokoz.vkconversations.di.Injector;
import me.agolokoz.vkconversations.util.AdapterItemClickListener;

public class ConversationsListFragment extends Fragment implements
        AdapterItemClickListener,
        ConversationsListContract.View {

    public static ConversationsListFragment createInstance() {
        return new ConversationsListFragment();
    }

    private ConversationsListContract.Presenter presenter;
    private ConversationsAdapter adapter;

    private View loadingView;
    private RecyclerView recyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        adapter = new ConversationsAdapter(this);
        setPresenter(new ConversationListPresenter(
                Injector.getMessagesRepository(),
                Injector.getUserRepository(),
                Injector.getSchedulerProvider(),
                this,
                context
        ));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.screen_conversations_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.conversationsListRecyclerView);
        recyclerView.setAdapter(adapter);
        loadingView = view.findViewById(R.id.conversationsListLoadingView);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.setAppNavigator(((MainActivity) getActivity()).getAppNavigator());
        presenter.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.stop();
        presenter.setAppNavigator(null);
    }

    @Override
    public void setPresenter(ConversationsListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void addConversations(List<ChatInfo> chatInfos) {
        adapter.addData(chatInfos);
        loadingView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateConversation(int position) {
        adapter.updateItem(position);
    }

    @Override
    public void onItemClicked(int position) {
        presenter.onConversationItemClicked(position);
    }
}
