package me.agolokoz.vkconversations.screen.list;

import java.util.List;

import me.agolokoz.domain.model.chat.info.ChatInfo;
import me.agolokoz.vkconversations.BasePresenter;
import me.agolokoz.vkconversations.BaseView;
import me.agolokoz.vkconversations.activity.navigator.AppNavigator;

public interface ConversationsListContract {

    interface Presenter extends BasePresenter {

        void loadConversations();
        void conversationChanged(int position);
        void onConversationItemClicked(int position);
        void setAppNavigator(AppNavigator appNavigator);
    }

    interface View extends BaseView<Presenter> {

        void addConversations(List<ChatInfo> chatInfos);
        void updateConversation(int position);
    }

}
