package me.agolokoz.vkconversations.screen.chat.handler;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.SparseArray;

import com.facebook.common.executors.UiThreadImmediateExecutorService;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.HashSet;
import java.util.Set;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import me.agolokoz.data.util.PhotoSizeUtil;
import me.agolokoz.domain.repository.UserRepository;
import me.agolokoz.vkconversations.screen.chat.ChatContract;
import me.agolokoz.vkconversations.util.AbstractPresenter;
import me.agolokoz.vkconversations.util.BitmapUtil;

public class ChatCompositePhotoHandler {

    @NonNull private final AbstractPresenter observablesHandler;
    @NonNull private final UserRepository userRepository;
    @NonNull private final ChatContract.Presenter presenter;
    @NonNull private final Context context;

    @NonNull private final ImagePipeline frescoPipeline;
    @NonNull private final SparseArray<Bitmap> userIdBitmapMap;
    private int[] userIds;
    private final int photoSize;

    public ChatCompositePhotoHandler(
            @NonNull AbstractPresenter observablesHandler,
            @NonNull UserRepository userRepository,
            @NonNull ChatContract.Presenter presenter,
            @NonNull Context context
    ) {
        this.observablesHandler = observablesHandler;
        this.userRepository = userRepository;
        this.presenter = presenter;
        this.context = context;

        this.frescoPipeline = Fresco.getImagePipeline();
        this.userIdBitmapMap = new SparseArray<>();
        this.photoSize = PhotoSizeUtil.getUserPhotoSizeFromDensity(context.getResources().getDisplayMetrics().density);
    }

    public void loadCompositePhoto(int[] firstUserIds) {
        userIds = firstUserIds;
        Set<Integer> userIdSet = new HashSet<>();
        for (int firstUserId : firstUserIds) {
            userIdSet.add(firstUserId);
        }

        Disposable disposable = observablesHandler.prepareObservable(userRepository.requestUsersPhotoUri(userIdSet))
                .subscribe(getUriMapConsumer(), observablesHandler.errorConsumer);
        observablesHandler.addDisposable(disposable);
    }

    private void onUrisReceived(SparseArray<Uri> uriMap) {
        for (int i = 0; i != uriMap.size(); ++i) {
            int key = uriMap.keyAt(i);
            requestPhoto(key, uriMap.get(key));
        }
    }

    private void requestPhoto(final int userId, Uri uri) {
        if (uri == null) {
            onPhotoReceived(userId, BitmapUtil.getDeactivatedBitmap(context));
        }
        else {
            ImageRequest imageRequest = ImageRequestBuilder
                    .newBuilderWithSource(uri)
                    .build();

            DataSource<CloseableReference<CloseableImage>> dataSource =
                    frescoPipeline.isInBitmapMemoryCache(imageRequest)
                            ? frescoPipeline.fetchImageFromBitmapCache(imageRequest, null)
                            : frescoPipeline.fetchDecodedImage(imageRequest, null);

            dataSource.subscribe(new BaseBitmapDataSubscriber() {
                @Override protected void onNewResultImpl(Bitmap bitmap) { onPhotoReceived(userId, bitmap); }
                @Override protected void onFailureImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {
                    if (dataSource != null && dataSource.getFailureCause() != null) dataSource.getFailureCause().printStackTrace();
                }
            }, UiThreadImmediateExecutorService.getInstance());
        }
    }

    private void onPhotoReceived(int userId, Bitmap bitmap) {
        userIdBitmapMap.put(userId, bitmap);

        boolean isOneUserPhotoForCurrentChatLoaded = false;
        for (int i = 0; i != userIds.length; ++i) {
            if (userIdBitmapMap.get(userIds[i], null) != null) {
                isOneUserPhotoForCurrentChatLoaded = true;
                break;
            }
        }

        if (isOneUserPhotoForCurrentChatLoaded) {
            Bitmap compositeBitmap = getCompositeBitmap(userIds);
            RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), compositeBitmap);
            drawable.setCornerRadius(compositeBitmap.getWidth() / 2);
            presenter.updateChatImage(drawable);
        }
    }

    private Bitmap getCompositeBitmap(int[] userIds) {
        int usersCount = userIds.length;
        Bitmap[] bitmaps = new Bitmap[usersCount];
        for (int i = 0; i != usersCount; ++i) {
            bitmaps[i] = userIdBitmapMap.get(userIds[i], null);
            if (bitmaps[i] == null) {
                bitmaps[i] = BitmapUtil.getEmptyBitmap(context);
            }
        }

        return BitmapUtil.getCompositeBitmap(context, bitmaps, photoSize);
    }


    private Consumer<SparseArray<Uri>> uriMapConsumer;
    private Consumer<SparseArray<Uri>> getUriMapConsumer() {
        if (uriMapConsumer == null) {
            uriMapConsumer = new Consumer<SparseArray<Uri>>() {
                @Override
                public void accept(SparseArray<Uri> uriSparseArray) throws Exception {
                    onUrisReceived(uriSparseArray);
                }
            };
        }
        return uriMapConsumer;
    }

}
