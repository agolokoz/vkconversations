package me.agolokoz.vkconversations.screen.chat.handler;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.SparseArray;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import me.agolokoz.domain.model.chat.message.Message;
import me.agolokoz.domain.repository.UserRepository;
import me.agolokoz.vkconversations.screen.chat.ChatContract;
import me.agolokoz.vkconversations.util.AbstractPresenter;

public class ChatUserPhotoHandler {

    @NonNull private final AbstractPresenter observablesHandler;
    @NonNull private final UserRepository userRepository;
    @NonNull private final ChatContract.Presenter presenter;
    private final List<Message> historyList;

    public ChatUserPhotoHandler(
            @NonNull AbstractPresenter observablesHandler,
            @NonNull UserRepository userRepository,
            @NonNull ChatContract.Presenter presenter,
            List<Message> historyList
    ) {
        this.observablesHandler = observablesHandler;
        this.userRepository = userRepository;
        this.presenter = presenter;
        this.historyList = historyList;
    }

    public void handle(List<Message> newMessages) {
        Set<Integer> userIdSet = new HashSet<>();
        for (Message message : newMessages) {
            userIdSet.add(message.getSenderId());
        }

        Disposable disposable = observablesHandler
                .prepareObservable(userRepository.requestUsersPhotoUri(userIdSet))
                .subscribe(getUserPhotoUrisConsumer(), observablesHandler.errorConsumer);
        observablesHandler.addDisposable(disposable);
    }

    private void onUserModelsLoaded(SparseArray<Uri> userIdPhotoUriMap) {
        for (int i = 0; i != historyList.size(); ++i) {
            Message history = historyList.get(i);
            if (history.isFromUser() || history.getSenderPhotoUri() != null) {
                continue;
            }

            Uri uri = userIdPhotoUriMap.get(history.getSenderId());
            history.setSenderPhotoUri(uri);
            presenter.updateItem(i);
        }
    }

    private Consumer<SparseArray<Uri>> userPhotoUrisConsumer;
    private Consumer<SparseArray<Uri>> getUserPhotoUrisConsumer() {
        if (userPhotoUrisConsumer == null) {
            userPhotoUrisConsumer = new Consumer<SparseArray<Uri>>() {
                @Override
                public void accept(SparseArray<Uri> userModels) throws Exception {
                    onUserModelsLoaded(userModels);
                }
            };
        }
        return userPhotoUrisConsumer;
    }

}
