package me.agolokoz.vkconversations.screen.chat.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

class LoadingViewHolder extends RecyclerView.ViewHolder {

    LoadingViewHolder(View itemView) {
        super(itemView);
    }
}