package me.agolokoz.vkconversations.screen.list;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.SparseArray;

import com.facebook.common.executors.UiThreadImmediateExecutorService;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import me.agolokoz.data.util.PhotoSizeUtil;
import me.agolokoz.domain.model.chat.info.ChatInfo;
import me.agolokoz.domain.repository.UserRepository;
import me.agolokoz.vkconversations.util.AbstractPresenter;
import me.agolokoz.vkconversations.util.BitmapUtil;

public class ConversationPhotoHandler {

    @NonNull private final AbstractPresenter observablesHandler;
    @NonNull private final UserRepository userRepository;
    @NonNull private final ConversationsListContract.Presenter presenter;
    @NonNull private final List<ChatInfo> chatInfoList;
    @NonNull private final Context context;

    @NonNull private final ImagePipeline frescoPipeline;
    @NonNull private final SparseArray<Bitmap> userIdBitmapMap;
    private final int photoSize;

    ConversationPhotoHandler(
            @NonNull AbstractPresenter observablesHandler,
            @NonNull UserRepository userRepository,
            @NonNull ConversationsListContract.Presenter presenter,
            @NonNull List<ChatInfo> chatInfoList,
            @NonNull Context context
    ) {
        this.observablesHandler = observablesHandler;
        this.userRepository = userRepository;
        this.presenter = presenter;
        this.chatInfoList = chatInfoList;
        this.context = context.getApplicationContext();

        this.frescoPipeline = Fresco.getImagePipeline();
        this.userIdBitmapMap = new SparseArray<>();
        this.photoSize = PhotoSizeUtil.getUserPhotoSizeFromDensity(context.getResources().getDisplayMetrics().density);
    }

    void requestCompositePhotos() {
        Set<Integer> userIdSet = new HashSet<>();
        for (int i = chatInfoList.size()-1; i >= 0; --i) {
            for (int userId : chatInfoList.get(i).getFirstUserIds()) {
                userIdSet.add(userId);
            }
        }

        Disposable d = observablesHandler.prepareObservable(userRepository.requestUsersPhotoUri(userIdSet))
                .subscribe(getUriMapConsumer(), observablesHandler.errorConsumer);
        observablesHandler.addDisposable(d);
    }

    private void onUrisReceived(SparseArray<Uri> uriMap) {
        for (int i = 0; i != uriMap.size(); ++i) {
            int key = uriMap.keyAt(i);
            requestPhoto(key, uriMap.get(key));
        }
    }

    private void requestPhoto(final int userId, Uri uri) {
        if (uri == null) {
            onPhotoReceived(userId, BitmapUtil.getDeactivatedBitmap(context));
        }
        else {
            ImageRequest imageRequest = ImageRequestBuilder
                    .newBuilderWithSource(uri)
                    .build();

            DataSource<CloseableReference<CloseableImage>> dataSource =
                    frescoPipeline.isInBitmapMemoryCache(imageRequest)
                            ? frescoPipeline.fetchImageFromBitmapCache(imageRequest, null)
                            : frescoPipeline.fetchDecodedImage(imageRequest, null);

            dataSource.subscribe(new BaseBitmapDataSubscriber() {
                @Override protected void onNewResultImpl(Bitmap bitmap) { onPhotoReceived(userId, bitmap); }
                @Override protected void onFailureImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {
                    if (dataSource != null && dataSource.getFailureCause() != null) dataSource.getFailureCause().printStackTrace();
                }
            }, UiThreadImmediateExecutorService.getInstance());
        }
    }

    private void onPhotoReceived(int userId, Bitmap bitmap) {
        userIdBitmapMap.put(userId, bitmap);

        for (int i = 0; i != chatInfoList.size(); ++i) {
            ChatInfo chatInfo = chatInfoList.get(i);
            if (chatInfo.getPhotoUri() != null) {
                continue;
            }

            boolean isAllUserPhotoForCurrentConvLoaded = true;
            for (int j = 0; j != chatInfo.getFirstUserIds().length; ++j) {
                if (userIdBitmapMap.get(chatInfo.getFirstUserIds()[j], null) == null) {
                    isAllUserPhotoForCurrentConvLoaded = false;
                    break;
                }
            }

            if (isAllUserPhotoForCurrentConvLoaded) {
                Bitmap compositeBitmap = getCompositeBitmap(chatInfo.getFirstUserIds());
                RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), compositeBitmap);
                drawable.setCornerRadius(compositeBitmap.getWidth() / 2);
                chatInfo.setDrawable(drawable);
                presenter.conversationChanged(i);
            }
        }
    }

    private Bitmap getCompositeBitmap(int[] userIds) {
        int usersCount = userIds.length;
        Bitmap[] bitmaps = new Bitmap[usersCount];
        for (int i = 0; i != usersCount; ++i) {
            bitmaps[i] = userIdBitmapMap.get(userIds[i], null);
            if (bitmaps[i] == null) {
                bitmaps[i] = BitmapUtil.getEmptyBitmap(context);
            }
        }

        return BitmapUtil.getCompositeBitmap(context, bitmaps, photoSize);
    }


    private Consumer<SparseArray<Uri>> uriMapConsumer;
    private Consumer<SparseArray<Uri>> getUriMapConsumer() {
        if (uriMapConsumer == null) {
            uriMapConsumer = new Consumer<SparseArray<Uri>>() {
                @Override
                public void accept(SparseArray<Uri> uriSparseArray) throws Exception {
                    onUrisReceived(uriSparseArray);
                }
            };
        }
        return uriMapConsumer;
    }

}
