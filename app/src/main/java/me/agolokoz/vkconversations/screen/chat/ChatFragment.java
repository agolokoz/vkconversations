package me.agolokoz.vkconversations.screen.chat;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import me.agolokoz.domain.model.chat.message.Message;
import me.agolokoz.vkconversations.R;
import me.agolokoz.vkconversations.di.Injector;
import me.agolokoz.vkconversations.screen.chat.adapter.ChatAdapter;
import me.agolokoz.vkconversations.util.PaginationListener;

public class ChatFragment extends Fragment implements ChatContract.View, PaginationListener {

    private static final String bundleKeyConversationId = "chatId";

    public static ChatFragment createInstance(int conversationId) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putInt(bundleKeyConversationId, conversationId);
        fragment.setArguments(args);
        return fragment;
    }

    private ChatContract.Presenter presenter;
    private ChatAdapter adapter;
    private RecyclerView recyclerView;
    private View loadingView;
    private ImageView chatPhotoImageView;
    private ActionBar actionBar;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        adapter = new ChatAdapter(this);
        setPresenter(new ChatPresenter(
                Injector.getMessagesRepository(),
                Injector.getUserRepository(),
                Injector.getSchedulerProvider(),
                this,
                context,
                getArguments().getInt(bundleKeyConversationId)
        ));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.screen_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.conversationRecyclerView);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        layoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(layoutManager);

        Toolbar toolbar = view.findViewById(R.id.conversationToolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        loadingView = view.findViewById(R.id.conversationLoadingView);
        chatPhotoImageView = view.findViewById(R.id.conversationPhotoImage);

        presenter.start();
    }

    @Override
    public void setLoadingState() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setLoadedState() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void addHistoryItems(List<Message> items, int totalItemsCount) {
        adapter.addData(items, totalItemsCount);
        if (adapter.getDataCount() == items.size()) {
            recyclerView.scrollToPosition(0);
            adapter.setPaginable(true);
        }
    }

    @Override
    public void clearHistoryItems() {
        adapter.clear();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPresenter(ChatContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void updateItem(int position) {
        adapter.updateItem(position);
    }

    @Override
    public void setConversationInfo(String title, int participantsCount, @Nullable Uri photoUri) {
        actionBar.setTitle(title);
        String participantsString = String.format(Locale.getDefault(), "%d %s",
                participantsCount,
                getContext().getString(R.string.conversation_participants)
        );
        actionBar.setSubtitle(participantsString);

        if (photoUri != null) {
            chatPhotoImageView.setImageURI(photoUri);
        }
    }

    @Override
    public void setConversationDrawable(Drawable drawable) {
        chatPhotoImageView.setImageDrawable(drawable);
    }

    @Override
    public void onPagination(int offset) {
        presenter.loadHistory(offset);
    }
}
