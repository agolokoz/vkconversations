package me.agolokoz.domain.model.chat.info;

import java.util.List;

public class ChatsResponse {

    private final int total;
    private final int skipped;
    private final List<ChatInfo> chatInfoList;

    public ChatsResponse(int total, int skipped, List<ChatInfo> chatInfoList) {
        this.total = total;
        this.skipped = skipped;
        this.chatInfoList = chatInfoList;
    }

    public int getTotal() {
        return total;
    }

    public int getSkipped() {
        return skipped;
    }

    public List<ChatInfo> getChatInfoList() {
        return chatInfoList;
    }
}
