package me.agolokoz.domain.model.chat.info;

import android.graphics.drawable.Drawable;
import android.net.Uri;

public class ChatInfo {

    private int id;
    private String title;
    private String lastMessage;
    private String lastMessageTime;
    private int[] firstUserIds;
    private int participantsCount;

    private Uri photoUri;
    private Drawable drawable;

    public ChatInfo(int id, String title, String lastMessage, String lastMessageTime, int[] firstUserIds, int participantsCount) {
        this.id = id;
        this.title = title;
        this.lastMessage = lastMessage;
        this.lastMessageTime = lastMessageTime;
        this.firstUserIds = firstUserIds;
        this.participantsCount = participantsCount;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public int getParticipantsCount() {
        return participantsCount;
    }

    public int[] getFirstUserIds() {
        return firstUserIds;
    }


    public Uri getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(Uri photoUri) {
        this.photoUri = photoUri;
    }


    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}
