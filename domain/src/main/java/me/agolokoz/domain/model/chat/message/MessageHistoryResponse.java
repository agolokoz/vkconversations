package me.agolokoz.domain.model.chat.message;

import java.util.List;

public class MessageHistoryResponse {

    private final int count;
    private final int itemsSkipped;
    private final List<Message> items;

    public MessageHistoryResponse(int count, int itemsSkipped, List<Message> items) {
        this.count = count;
        this.itemsSkipped = itemsSkipped;
        this.items = items;
    }

    public int getCount() {
        return count;
    }

    public int getItemsSkipped() {
        return itemsSkipped;
    }

    public List<Message> getItems() {
        return items;
    }
}
