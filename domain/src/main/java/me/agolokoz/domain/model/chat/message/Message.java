package me.agolokoz.domain.model.chat.message;

import android.net.Uri;

import me.agolokoz.domain.model.chat.message.attachment.MessageAttachment;

public class Message {

    private final int senderId;
    private final boolean isFromUser;
    private final String text;
    private final String timeString;
    private final MessageAttachment attachment;

    private Uri senderPhotoUri;

    public Message(int senderId, boolean isFromUser, String text, String timeString, MessageAttachment attachment) {
        this.senderId = senderId;
        this.isFromUser = isFromUser;
        this.text = text;
        this.timeString = timeString;
        this.attachment = attachment;
    }

    public int getSenderId() {
        return senderId;
    }

    public boolean isFromUser() {
        return isFromUser;
    }

    public String getText() {
        return text;
    }

    public String getTimeString() {
        return timeString;
    }

    public MessageAttachment getAttachment() {
        return attachment;
    }


    public Uri getSenderPhotoUri() {
        return senderPhotoUri;
    }

    public void setSenderPhotoUri(Uri senderPhotoUri) {
        this.senderPhotoUri = senderPhotoUri;
    }
}
