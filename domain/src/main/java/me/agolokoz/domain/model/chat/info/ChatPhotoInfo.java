package me.agolokoz.domain.model.chat.info;

import android.net.Uri;

public class ChatPhotoInfo {

    private final int conversationId;
    private final Uri photoUri;

    public ChatPhotoInfo(int conversationId, Uri photoUri) {
        this.conversationId = conversationId;
        this.photoUri = photoUri;
    }

    public int getConversationId() {
        return conversationId;
    }

    public Uri getPhotoUri() {
        return photoUri;
    }
}
