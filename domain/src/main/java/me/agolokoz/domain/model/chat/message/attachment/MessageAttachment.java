package me.agolokoz.domain.model.chat.message.attachment;

import android.net.Uri;

public class MessageAttachment {

    public Uri photoUri;
    public int width;
    public int height;

    public MessageAttachment(Uri photoUri, int width, int height) {
        this.photoUri = photoUri;
        this.width = width;
        this.height = height;
    }

    public Uri getPhotoUri() {
        return photoUri;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
