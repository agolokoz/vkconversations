package me.agolokoz.domain.model.user;

public class UserModel {

    private final long id;
    private final String photo;

    public UserModel(long id, String photo) {
        this.id = id;
        this.photo = photo;
    }

    public long getId() {
        return id;
    }

    public String getPhoto() {
        return photo;
    }
}
