package me.agolokoz.domain.util;

import android.support.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    private static final SimpleDateFormat HHmm = new SimpleDateFormat("HH:mm", Locale.getDefault());

    @Nullable
    public static String transformDateToHHmmString(Date date) {
        try {
            return HHmm.format(date);
        } catch (RuntimeException e) {
            return null;
        }
    }

}
