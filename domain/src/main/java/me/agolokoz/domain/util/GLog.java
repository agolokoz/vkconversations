package me.agolokoz.domain.util;

import android.util.Log;

public class GLog {

    private static final String tag = "VKConversations";

    public static void d(String message) {
        Log.d(tag, message);
    }

}
