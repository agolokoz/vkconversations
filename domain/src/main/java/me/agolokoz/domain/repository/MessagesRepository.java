package me.agolokoz.domain.repository;

import android.net.Uri;
import android.util.SparseArray;

import io.reactivex.Observable;
import me.agolokoz.domain.model.chat.info.ChatInfo;
import me.agolokoz.domain.model.chat.info.ChatsResponse;
import me.agolokoz.domain.model.chat.message.MessageHistoryResponse;

public interface MessagesRepository {

    Observable<ChatsResponse> requestConversations(int offset);
    Observable<SparseArray<Uri>> requestConversationsPhotoUri(int[] conversationsIds);
    Observable<ChatInfo> requestChatInfo(int chatId);
    Observable<MessageHistoryResponse> requestChatHistory(int chatId, int offset);
}
