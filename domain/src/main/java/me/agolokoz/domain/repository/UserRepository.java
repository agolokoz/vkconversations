package me.agolokoz.domain.repository;

import android.net.Uri;
import android.util.SparseArray;

import java.util.Set;

import io.reactivex.Observable;

public interface UserRepository {

    Observable<SparseArray<Uri>> requestUsersPhotoUri(Set<Integer> userIds);
}
